from PIL import Image, ExifTags
import os
import pandas as pd
import calendar
import datetime
import numpy as np
import logging
from Levenshtein import StringMatcher
import re

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('[%(levelname)s] / %(name)s->%(message)s')
file_handler = logging.FileHandler('run.log')
file_handler.setFormatter(formatter)

stream_handler = logging.StreamHandler()
format_stream = logging.Formatter('[%(levelname)s]->%(message)s')
stream_handler.setFormatter(format_stream)

logger.addHandler(stream_handler)
logger.addHandler(file_handler)


def filter_digit_str(string):
    """removes digits found in string

    Parameters
    ----------
    string : str
        input string

    Returns
    -------
    str
        string without digits
    """
    new_string = ""
    for char in string:
        if not char.isdigit():
            new_string += char

    return clean_str(new_string)


def extract_digit_str(string):
    """extract digits and white spaces from string

    Parameters
    ----------
    string : str
        input string

    Returns
    -------
    str
        string of digits found in 'string'
    """
    digit = ""
    for char in string:
        if char.isdigit() or char == " ":
            digit += char
    return clean_str(digit.split(" ")[-1])


def parse_datetime_input(datetime_input):
    """inserts '%' symbole in datetime format
       required because configparser doesnt handle '%'.
       ex: 'Y-m-d-H-M-S' yields '%Y-%m-%d-%H-%M-%S'

    Parameters
    ----------
    datetime_input : str
        datetime desired format without '%'
        ex: 'Y-m-d-H-M-S'

    Returns
    -------
    str
        input with inserted '%'
    """
    parsed = ""
    for letter in datetime_input:
        if letter.isalpha():
            parsed += "%"+letter
        else:
            parsed += letter

    return parsed


def pic_infos_from_folder(pic_folder, config):
    """return list of pic caption time from pic folder

    Parameters
    ----------
    pic_folder : str
        path to folder of pics
    config : configparser.ConfigParser
        ConfigParser instance where read() method has already been
        called on configuration file.

    Returns
    -------
    list of str
        list of pic caption time
    """
    pic_date_times = []
    photo_formats = config["SPECIFICATIONS"]["photo formats"].split(" ")
    pic_paths = get_photo_path(pic_folder, photo_formats)
    for path in pic_paths:
        date_time = get_pic_datetime(path, config)
        pic_date_times.append(date_time)
    return pic_date_times


def get_photo_path(path, photo_formats):
    """retrieve the paths of all files who's extention is in 'specs'

    Parameters
    ----------
    path : str
        path to root photo folder
    photo_formats : list of str
        list of allowed photo formats
        ex: ['.jpeg', '.png']

    Returns
    -------
    list of str
        list of photo absolute path
    """
    photo_paths = []
    for dirpath, _, files in os.walk(path):
        for file in files:
            if os.path.splitext(file)[-1] in photo_formats:
                file_path = os.path.join(dirpath, file)
                file_path = os.path.abspath(file_path)
                photo_paths.append(file_path)
    return photo_paths


def parse_folder_name(folder_path, config):
    """extracts photo metadata from folder name

    Parameters
    ----------
    folder_path : str
        folder path
    config : configparser.ConfigParser
        ConfigParser instance where read() method has already been
        called on configuration file.

    Returns
    -------
    tulpe (str, list of str, list of str)
        return animal name, list of caption dates, list of locations

    Raises
    ------
    IOError
        [description]
    """

    folder_name = os.path.basename(folder_path)
    animal_name = folder_name.split("-")[0]
    animal_name = animal_name.replace(os.path.dirname(folder_name), "")
    animal_name = clean_str(animal_name)

    if has_subfolders(folder_path):
        locations = []
        pic_dates = []
        for f in get_subfolders(folder_path):
            pic_folder_path = os.path.join(folder_path, f)
            pic_date = pic_infos_from_folder(
                os.path.join(folder_path, f), config)
            pic_dates += pic_date
            locations += parse_locations(pic_folder_path,
                                         animal_name)*len(pic_date)

    else:
        pic_dates = pic_infos_from_folder(folder_path, config)
        splitted_name = folder_name.split("-")
        if len(splitted_name) == 2:
            locations = clean_str(splitted_name[1].split(';')[0])
        else:
            logger.warning("[LOCATION] FAILED TO PARSE FOR [{}] ==> FOLDER:[{}]".format(
                folder_path, animal_name))
            locations = np.nan

    return animal_name, pic_dates, locations


def parse_locations(pic_folder_path, animal_name):
    """Extracts location data from folder name


    Parameters
    ----------
    pic_folder_name : str
        path to folder
    animal_name : str
        name of animal on the photo

    Returns
    -------
    list of str
        return list of locations found
    """

    pic_folder_name = os.path.basename(pic_folder_path)
    # CANT HAVE - IN FOLDER NAME
    if "-" in pic_folder_name:
        location = pic_folder_name.split("-")[-1]
    else:

        animal_name = animal_name.replace(
            "(", "").replace(")", "")  # cf odonate
        pic_folder_name = clean_str(pic_folder_name)
        animal_list = animal_name.split(" ")
        folder_name_list = pic_folder_name.split(" ")
        levenst_thresh = 2
        len_animal = len(animal_name)
        location = np.nan
        if len_animal == 1:

            similarity = [StringMatcher.distance(
                pic_folder_name[i:len_animal+i],
                animal_name) for i in range(len(pic_folder_name)-len_animal)]

            dist_min = min(similarity)
            index = similarity.index(dist_min)
            if dist_min <= levenst_thresh:
                location = pic_folder_name[index+len_animal:]

        else:  # animal name has at least one white space
            indexes = []
            dists = []
            for name in animal_list:
                similarity = [StringMatcher.distance(i, name)
                              for i in folder_name_list]
                dist = min(similarity)
                index = similarity.index(dist)
                if dist <= levenst_thresh:
                    indexes.append(index)
                    dists.append(dist)
            if len(indexes) >= len(animal_list)-1 and len(indexes) > 0:
                max_index = max(indexes)
                try:
                    location = " ".join(folder_name_list[max_index+1:])
                except IndexError:
                    pass
        if location is np.nan:
            logger.warning("[LOCATION] FAILED TO PARSE FOR [{}] ==> FOLDER:[{}]".format(
                pic_folder_path, animal_name))

    if location is not np.nan:
        location = clean_str(location.split(';')[0])
    return [location]


def get_subfolders(folder_path):
    """returns list of subfolder found in 'folder_path'

    Parameters
    ----------
    folder_path : str
        path to a folder

    Returns
    -------
    list of str
        list of subfolder names found
    """
    content = os.listdir(folder_path)
    subfolders = [i for i in content if os.path.isdir(
        os.path.join(folder_path, i))]
    return subfolders


def get_subfiles(folder_path):
    """returns list of files found in 'folder_path'

    Parameters
    ----------
    folder_path : str
        path to folder

    Returns
    -------
    list of str
        list of files found
    """
    content = os.listdir(folder_path)
    subfiles = [i for i in content if os.path.isfile(
        os.path.join(folder_path, i))]
    return subfiles


def has_subfolders(folder):
    """check if folder has subfolders
    Parameters
    ----------
    folder : str
        path to folder

    Returns
    -------
    bool
        return True if folder has  at least one subfolder
    """
    content = os.listdir(folder)
    for i in content:
        if os.path.isdir(os.path.join(folder, i)):
            return True
    return False


def get_pic_datetime(pic_path, config):
    """return date time meta data of picture at path 'pic_data'

    Parameters
    ----------
    pic_path : str
        path to picture
    config : configparser.ConfigParser
        ConfigParser instance where read() method has already been
        called on configuration file.

    Returns
    -------
    str
        datetime when the picture was taken
    """
    pic_ext = os.path.splitext(pic_path)[-1]
    pic_formats = config["SPECIFICATIONS"]["photo formats"].split(" ")

    if pic_ext.lower() not in pic_formats:
        return None

    img = Image.open(pic_path)
    meta_data = "DateTimeOriginal"
    exifTagId = 36867

    name = ExifTags.TAGS[exifTagId]
    if name != meta_data:
        logger.error("[META DATA] meta data name for {} is {}, expected {}".format(
            pic_path, name, meta_data))

    try:
        pic_date = img._getexif()[exifTagId]
    except (KeyError, TypeError):
        logger.error(
            "[META DATA] Failed to get meta data from {} ".format(pic_path))
        pic_date = None
    return pic_date


def clean_str(exp):
    """
    Remove unwanted trailing characters

    Parameters
    ----------
    exp : str or list
        expression to remove special characters

    Returns
    -------
    same type as input
        cleaned expression
    """
    filter_trailing_chars = '\\ -_?!/:;,'
    if type(exp) == str:
        exp = exp.strip(filter_trailing_chars).\
            replace("  ", " ")
    elif type(exp) == list:
        try:
            exp = list(map(lambda x: x.strip(
                filter_trailing_chars).replace("  ", " "), exp))
        except AttributeError:
            pass
    return exp


def df_from_subfolders(parent, config):
    """ Creates dataframe from pic data found in 'folders'

    Parameters
    ----------
    parent : str
        path to root photo folder
    config : configparser.ConfigParser
        ConfigParser instance where read() method has already been
        called on configuration file.

    Returns
    -------
    pd.DataFrame
        Dataframe containing pic data
    """
    logger.info("Collecting data from Folder...\n")
    df = pd.DataFrame({})
    folders = get_subfolders(parent)

    for folder in folders:
        logger.info("\n Parsing {} \n".format(folder))
        folder_path = os.path.join(parent, folder)
        animal, dates, locations = parse_folder_name(folder_path, config)
        temp = pd.DataFrame({"Nom": animal,
                             "Localisation": locations,
                             "DateTime": pd.to_datetime(dates,
                                                        format=("%Y:%m:%d %H:%M:%S"))})  # this format is from jpeg metadata

        df = pd.concat((df, temp), ignore_index=True)

    # drop photos duplicate dates, casting names to categorical value
    # Put this in function soon
    df["date"] = pd.to_datetime(
        df.DateTime.dt.date, infer_datetime_format=True)
    df.drop_duplicates(subset=("date", "Nom"), inplace=True)

    df.loc[:, "Localisation"] = df["Localisation"].fillna("Inconnue")
    df.loc[:, "Dept"] = df["Localisation"].apply(
        lambda x: extract_digit_str(x))
    df.loc[:, "Dept"] = df["Dept"].replace("", np.nan)
    df.loc[:, "Localisation"] = df["Localisation"].apply(
        lambda row: filter_digit_str(row))

    df.loc[:, "Localisation"] = df["Localisation"].astype("category")
    df.loc[:, "Nom"] = df["Nom"].astype("category")

    return df


def cache_data(df, parent, config):
    """pickles dataframe 'df'

    Parameters
    ----------
    df : pd.DataFrame
        dataframe containing photo metadata
    parent : str
        path to root photo folder
    config : configparser.ConfigParser
        ConfigParser instance where read() method has already been
        called on configuration file.

    Returns
    -------
    None
    """

    pickle_path = build_cache_path(parent, config)
    df.to_pickle(pickle_path)
    logger.info("Cached data at: "+pickle_path+"\n")


def build_cache_path(parent, config):
    """builds cache file path

    Parameters
    ----------
    parent : str
        path to root photo folder
    config : configparser.ConfigParser
        ConfigParser instance where read() method has already been
        called on configuration file.

    Returns
    -------
    str
        path to pickle
    """
    pickle_dir = os.path.dirname(os.path.abspath(__file__))
    datetime_format = config["SPECIFICATIONS"]["datetime format"]
    datetime_format = parse_datetime_input(datetime_format)
    str_datetime = datetime.datetime.today().strftime(datetime_format)
    pickle_name = "cache_" + \
        os.path.basename(parent) + "_" + str_datetime + ".pickle"
    pickle_path = os.path.join(pickle_dir, pickle_name)
    return pickle_path


def group_animal(df, group="year"):
    """counts animal ocurrences in df by 'group'

    Parameters
    ----------
    df : pd.DataFrame
        dataframe containing photo metadata
    group : str, optional
        [description], by default "year"

    Returns
    -------
    pd.DataFrame
        return dataframe (Name, Count)
    """
    # cast category to str because of groupby bug
    for c in ["Nom", "Localisation", "Dept"]:
        df.loc[:, "Nom"] = df["Nom"].astype("str")
        df.loc[:, "Localisation"] = df["Localisation"].astype("str")

    if group == "year":
        group = df.date.dt.year
    elif group == "month":
        group = df.date.dt.month

    df_grouped = df.groupby(by=["Nom", group]).count()
    df_grouped = df_grouped.rename(columns={"date": "Count"})
    df_grouped = df_grouped.fillna(0)
    df_grouped = df_grouped.reset_index()
    return df_grouped


def num_to_month(nums):
    """Convert integer to month.

    Parameters
    ----------
    nums : list of integer
        Month number

    Returns
    -------
    list of str
        list of months
    """
    months = [calendar.month_name[int(i)] for i in nums]
    return months


def prepare_data(config):
    """This function calls all the other backend functions to:
        -Collect photo data and store them in DataFrame
        -Pickles DataFrame to avoid parsing photo folder everytime


    Parameters
    ----------
    config : configparser.ConfigParser()
        ConfigParser instance where read() method has already been
        called on configuration file.

    Returns
    -------
    pd.DataFrame
        DataFrame containing photo metadata
    """

    parent = os.path.abspath(config["PATHS"]["photo folder"])
    logger.info("Preparing Data from {} ... \n".format(
        os.path.basename(parent)))
    path = os.path.dirname(os.path.abspath(__file__))
    cache = get_latest_cache(path, parent, config)

    if cache is None:
        logger.info("No cache found, creating cache... \n")
        df = df_from_subfolders(parent, config)
        cache = cache_data(df, parent, config)

    else:
        cache_path = os.path.join(path, cache)
        df = update_cache(cache_path, parent, config)

    return df


def update_cache(cache_path, parent, config):
    """
    updates cache at 'cache_path'

    Parameters
    ----------
    cache_path : str
        path to cache file
    parent : str
        path to root photo folder
    config : configparser.ConfigParser()
        ConfigParser instance where read() method has already been
        called on configuration file.

    Returns
    -------
    pd.DataFrame
        return updated dataframe
    """
    cache_date_str = os.path.basename(cache_path).\
        split(".")[0].split("_")[-1]
    datetime_format = config["SPECIFICATIONS"]["datetime format"]
    datetime_format = parse_datetime_input(datetime_format)
    cache_date = datetime.datetime.strptime(
        cache_date_str, datetime_format)

    outdated_folders = []
    folders = get_subfolders(parent)
    for f in folders:
        f_date = last_modified_date(os.path.join(parent, f))
        if f_date > cache_date:
            outdated_folders.append(f)

    df_outdated = pd.read_pickle(cache_path)
    if len(outdated_folders) == 0:
        return df_outdated

    print("THESE FOLDER WERE MODIFIED AFTER CACHE!!!!!!!")
    logger.info("Folder {} were modified since previous cache ".
                format(outdated_folders))

    df_update = pd.read_pickle(cache_path)
    df = pd.concat((df_outdated, df_update))
    df.drop_duplicates(inplace=True, keep='last')
    os.remove(cache_path)
    new_cache_path = build_cache_path(parent, config)
    df.to_pickle(new_cache_path)
    return df


def get_latest_cache(path, parent, config):
    """Looks for cache pickle in same folder as main script
        returns latest cache
        path : str
        path to folder of main script

    Parameters
    ----------
    path : str
        path to folder of main script
    parent : str
        path to root photo folder
    config : configparser.ConfigParser
        instance of ConfigParser. read(config_file.ini) must have been called

    Returns
    -------
    None or str
        name of latest cache found (.pickle), None if no cache found
    """

    content = os.listdir(path)
    folder = os.path.basename(parent)
    caches_found = []
    for f in content:
        list_ = f.split(".")
        list_cache = list_[0].split("_")
        folder_name = "_".join(list_cache[1:-1])
        if list_[-1] == "pickle" and folder_name == folder:
            caches_found.append(f)

    if len(caches_found) == 0:
        return None
    else:
        # return most recent cache
        list_date_caches = []
        for c in caches_found:
            date_str = c.split(".")[0].split("_")[-1]
            try:
                datetime_format = config["SPECIFICATIONS"]["datetime format"]
                datetime_format = parse_datetime_input(datetime_format)
                cache_date = datetime.datetime.strptime(
                    date_str, datetime_format)
                list_date_caches.append((cache_date, c))
            except ValueError:
                logger.warning(
                    "Ignoring unrecognized cache name {} \n ".format(c))
                pass

    if len(list_date_caches) == 0:
        return None
    latest_cache = max(list_date_caches)[1]

    logger.info("Loading latest cache : {} \n".format(latest_cache))
    return latest_cache


def last_modified_date(dir_path):
    """return last modified date of folder at 'dir_path'

    Parameters
    ----------
    dir_path : [type]
        [description]

    Returns
    -------
    datetime.datetime
        last modified date of dir.

    Raises
    ------
    IOError
        if 'dir_path' doesnt point to a valid dir.
    """

    if os.path.isdir(dir_path):
        t = os.path.getmtime(dir_path)
        return datetime.datetime.fromtimestamp(t)
    else:
        raise IOError("must be a dir path")
