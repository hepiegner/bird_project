# Bird_project

This project is a visualization tool using **Dash** and **plotly** initially coded for my father to plot statistics of his animal photos.
The project is still under construction :).


# Features

The main features of this project are:

1. Scraps photo metadata (name of animal and location) from folder names
2. Stores all the metadata found in a dataframe and pickles it for later runs
3. If a cache file is found, only recently modified folders will be analysed to save runtime
4. The Dash interface provides dropdowns to filter data by `location`, `animal name`, `year` and `departement`

# Screenshot of Dash interface
![](images/Dashboard.png)

